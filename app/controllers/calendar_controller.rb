class CalendarController < ApplicationController
  def mainview
    @title = "All Events"
    @active = {}
    @og = {}
    event = Event
    if !params[:search].blank?
      @active[:search] = params[:search]
      event = PgSearch.multisearch params[:search].to_s
      event = Event.where(id:event.map{|s| s.searchable_id})
    end
    if !params[:calendar].blank?
      @active[:calendar] = Calendar.find(params[:calendar])
      event = event.where(calendar:params[:calendar])
    end
    if !params[:tag].blank?
      @active[:tag] = Tag.find(params[:tag])
      event = event.joins(:tags).where(tags: {id: params[:tag]})
    end
    if !params[:date].blank?
      @active[:date] = Date.strptime(params[:date], "%m/%d/%Y")
      event = event.where("dtstart >= ?",@active[:date])
    end
    @ev = formatEvents event
    render template: "calendar/mainview1"
  end
  def parse
    parseCalendar
    render json: {a:1}
  end
  def drop
    Event.delete_all
    Calendar.delete_all
    Tag.delete_all
    render json: {a:1}
  end
  private
    def formatEvents(events)
      events = events.order("dtstart, dtend").group_by{|e| e.dtstart.to_date}
      events.each do |k, events_in_date|
        events[k] = events_in_date.group_by do |e|
          [e.dtstart.to_time, e.dtend.to_time]
        end
      end
      events
    end
    def parseCalendar
      require 'open-uri'
      require 'icalendar'
      urls = ["https://www.google.com/calendar/ical/e9runcnq4ejd2vvj4tpgb2qhro%40group.calendar.google.com/public/basic.ics",
            "https://www.google.com/calendar/ical/e4q9nvb58fg52qipvire5uvin4%40group.calendar.google.com/public/basic.ics"
            ]

      urls.each do |url|
        cal_str = ""
        open(URI.parse(url)) do |data|
          cal_str = cal_str + data.read
        end
        cals = Icalendar.parse(cal_str)
        cal = cals.first
        if Calendar.where(url:url).blank?
          Calendar.new(
            name:cal.custom_properties["x_wr_calname"].first.to_s,
            url:url
          ).save
        end

        calendar = Calendar.where(url:url).first
        cal.events.each do |e|
          saved_e = Event.where(uid:e.uid.to_s).first
          ev_desc = e.description.to_s
          splat = ev_desc.partition('TAGS:')
          ev_desc, tags = splat.first, splat.last.split(',')

          event_info = {
            summary: e.summary.to_s,
            description: ev_desc,
            location: e.location.to_s,
            dtstart: e.dtstart,
            dtend: e.dtend,
            created: e.created,
            last_modified: e.last_modified
          }

          #logger.debug "#{!saved_e.blank?}"
          #This irks me so much!
          if !saved_e.blank?
            if saved_e.last_modified < e.last_modified
              saved_e.update(event_info)
              saved_e.tags.each { |t| saved_e.tags.delete(t) }
              tags.each do |t|
                saved_e.tags << Tag.find_or_create_by(name: t)
              end
            end
          end
          next if !saved_e.blank?

          cal_event = calendar.events.new(uid: e.uid.to_s)
          cal_event.update(event_info)
          tags.each do |t|
            cal_event.tags << Tag.find_or_create_by(name: t)
          end
        end
      end
    end
end
