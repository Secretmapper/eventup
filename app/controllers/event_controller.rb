class EventController < ApplicationController
  def edit
    @ev = event
    @active = {}
    @title = "Event: #{@ev.summary}"
    if params[:event] and params[:event]['image']
      hash = Cloudinary::Uploader.upload(params[:event]['image'])
      if @ev.attach.blank?
        @ev.attach = []
      end
      @ev.attach.push(hash['public_id'])
    end
    img_i = false
    img_i = params[:img_index] unless params[:img_index].blank?
    if img_i and (img_i.to_i.to_s==img_i) and @ev.attach and img_i.to_i <= @ev.attach.length - 1
      @ev.attach.delete_at(img_i.to_i)
    end
    @ev.save
    @og = {title: @ev.summary}
    @og[:image] = @ev.attach.first if @ev.attach and !@ev.attach.first.blank?
  end
#<%= link_to @ev.calendar.name, calendar_path(@ev.calendar.name), style:"font-size:15px" %>
  private
    def event
      @ev ||= Event.find(params[:id])
      @ev or not_found
    end
end
