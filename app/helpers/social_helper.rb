module SocialHelper
  def fb_tag(id)
    content_tag(:a,
        content_tag(:i, '', class: "fa fa-facebook") + " Share",
      class: "btn btn-xs btn-social btn-facebook",
      onclick:"postToFeed("+id.to_s+")")
  end
  def twitter_tag()
    %{<a class="btn btn-xs btn-social btn-twitter" >
      <i class="fa fa-twitter"></i> Tweet
    </a>}
    content_tag(:a,
        content_tag(:i, '', class:'fa fa-twitter') + "Tweet",
      class: "btn btn-xs btn-social btn-twitter")
  end
end
