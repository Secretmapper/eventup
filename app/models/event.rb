class Event < ActiveRecord::Base
  include PgSearch
  multisearchable against: [:description, :summary, :location]
  has_and_belongs_to_many :tags
  belongs_to :calendar
  serialize :attach
  def time
    st = self.dtstart.strftime("%l:%M %p")
    se = self.dtend.strftime("%l:%M %p")
    if st == se
      "All Day"
    else
      st.to_s + '-' + se.to_s
    end
  end
end
