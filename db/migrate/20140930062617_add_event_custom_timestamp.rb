class AddEventCustomTimestamp < ActiveRecord::Migration
  def change
      add_column :events, :created, :datetime
      add_column :events, :last_modified, :datetime
      add_index :events, :last_modified
  end
end
