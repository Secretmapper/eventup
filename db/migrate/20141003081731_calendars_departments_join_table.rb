class CalendarsDepartmentsJoinTable < ActiveRecord::Migration
  def change
    create_table :calendars_departments, id: false do |t|
      t.integer :calendar_id
      t.integer :department_id
    end

    add_index :calendars_departments, [:calendar_id, :department_id]
  end
end
