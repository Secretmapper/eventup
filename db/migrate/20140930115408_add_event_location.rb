class AddEventLocation < ActiveRecord::Migration
  def change
    add_column :events, :location, :string
    add_index :events, :location
  end
end
