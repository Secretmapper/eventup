class AddCalendarUrl < ActiveRecord::Migration
  def change
    add_column :calendars, :url, :string
    add_index :calendars, :url, unique: true
  end
end
