class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.references :calendar
      t.text :uid, null: false
      t.string :summary
      t.text :description
      t.text :attach
      t.datetime :dtstart
      t.datetime :dtend
    end
    add_index :events, :uid, unique: true
    add_index :events, :calendar_id
    change_column :events, :uid, :string
  end
end
