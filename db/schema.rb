# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141007063313) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"
  enable_extension "fuzzystrmatch"

  create_table "calendars", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "url"
  end

  add_index "calendars", ["name"], name: "index_calendars_on_name", using: :btree
  add_index "calendars", ["url"], name: "index_calendars_on_url", unique: true, using: :btree

  create_table "calendars_departments", id: false, force: true do |t|
    t.integer "calendar_id"
    t.integer "department_id"
  end

  add_index "calendars_departments", ["calendar_id", "department_id"], name: "index_calendars_departments_on_calendar_id_and_department_id", using: :btree

  create_table "departments", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: true do |t|
    t.integer  "calendar_id"
    t.string   "uid",           null: false
    t.string   "summary"
    t.text     "description"
    t.text     "attach"
    t.datetime "dtstart"
    t.datetime "dtend"
    t.datetime "created"
    t.datetime "last_modified"
    t.string   "location"
  end

  add_index "events", ["calendar_id"], name: "index_events_on_calendar_id", using: :btree
  add_index "events", ["last_modified"], name: "index_events_on_last_modified", using: :btree
  add_index "events", ["location"], name: "index_events_on_location", using: :btree
  add_index "events", ["uid"], name: "index_events_on_uid", unique: true, using: :btree

  create_table "events_tags", id: false, force: true do |t|
    t.integer "tag_id"
    t.integer "event_id"
  end

  add_index "events_tags", ["tag_id", "event_id"], name: "index_events_tags_on_tag_id_and_event_id", using: :btree

  create_table "pg_search_documents", force: true do |t|
    t.text     "content"
    t.integer  "searchable_id"
    t.string   "searchable_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "tags", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

end
